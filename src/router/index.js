import {
  createRouter,
  createWebHashHistory
} from 'vue-router'
import HomeView from '@/views/home/index.vue'
import { useUserStore } from '@/stores/user'
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView,
    children: [{
      path: '/',
      redirect: '/care'
    },
    {
      path: 'care',
      name: 'care',
      component: () => import('../views/home/care.vue')
    },
    {
      path: 'design',
      name: 'design',
      component: () => import('../views/home/design.vue')
    },
    {
      path: 'find',
      name: 'find',
      component: () => import('../views/home/find.vue')
    },

      // {
      //   path: 'find',
      //   name: 'find',
      //   component: () => import('../views/home/find.vue')
      // },
    ]
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      hidden: true
    },
    component: () => import('../views/login/index.vue')
  },
  {
    path: '/login2',
    name: 'login2',
    meta: {
      hidden: true
    },
    component: () => import('../views/login2/index.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: {
      hidden: true
    },
    component: () => import('../views/register/index.vue')
  },
  {
    path: '/friend',
    name: 'friend',
    component: () => import('../views/friend/index.vue'),
  },
  {
    path: '/getFriendActive',
    name: 'getFriendActive',
    meta: {
      hidden: true
    },
    component: () => import('../views/active/active.vue')
  },
  {
    path: '/getAppDetail',
    name: 'getAppDetail',
    meta: {
      hidden: true
    },
    component: () => import('../views/active/appDetail.vue')
  },
  {
    path: '/getApply',
    name: 'getApply',
    meta: {
      hidden: true
    },
    component: () => import('../views/active/apply.vue')
  },
  {
    path: '/authentication',
    name: 'authentication',
    meta: {
      hidden: true
    },
    component: () => import('../views/active/authentication.vue')
  },
  {
    path: '/getSubmit',
    name: 'getSubmit',
    meta: {
      hidden: true
    },
    component: () => import('../views/active/submit.vue')
  },
  {
    path: '/getScenery',
    name: 'getScenery',
    meta: {
      hidden: true
    },
    component: () => import('../views/scenery/scenery.vue')
  },
  {
    path: '/message',
    name: 'message',
    component: () => import('../views/message/Messageindex.vue'),
    children: [{
      path: 'messagetell',
      name: 'messagetell',
      component: () => import('../views/message/MessageTell.vue')
    },
    {
      path: 'messagegoto',
      name: 'messagegoto',
      component: () => import('../views/message/MessageGoto.vue')
    },
    ]
  },
  {
    path: '/my',
    name: 'my',
    component: () => import('../views/my/index.vue')
  },
  {
    path: '/trip',
    name: 'trip',
    component: () => import('../views/trip/index.vue'),
    children: [
      {
        path: 'tripedit',
        name: 'tripedit',
        meta: {
          hidden: true
        },
        component: () => import('../views/trip/tripedit.vue')
      },
      {
        path: 'tripdetail',
        name: 'tripdetail',
        meta: {
          hidden: true
        },
        component: () => import('../views/trip/tripdetail.vue')
      },
      {
        path: 'tripediting',
        name: 'tripediting',
        meta: {
          hidden: true
        },
        component: () => import('../views/trip/tripediting.vue')
      },
      {
        path: 'tripdetailing',
        name: 'tripdetailing',
        meta: {
          hidden: true
        },
        component: () => import('../views/trip/tripdetailing.vue')
      },
    ]
  },
  {
    path: '/details',
    name: 'details',
    component: () => import('../views/details/index.vue'),
    children: [{
      path: 'myorder',
      name: 'myorder',
      meta: {
        hidden: true
      },
      component: () => import('../views/details/order.vue'),

    },
    {
      path: 'mytrip',
      name: 'mytrip',
      meta: {
        hidden: true
      },
      component: () => import('../views/details/trip.vue'),
    },
    {
      path: 'editpassword',
      name: 'editpassword',
      component: () => import('../views/details/editpassword.vue'),
    },
    // {
    //   path: 'myperson',
    //   name: 'myperson',
    //   component: () => import('../views/details/person.vue'),
    // },
    {
      path: 'receive',
      name: 'receive',
      component: () => import('../views/details/receive.vue'),
      children: [
        {
          path: 'receivego',
          name: 'receivego',
          component: () => import('../views/details/receivego.vue'),
        },
        {
          path: 'receiveserve',
          name: 'receiveserve',
          component: () => import('../views/details/receiveserve.vue'),
        },
        
      ]
    },
    ]
  },
  {
    path: '/messagedetail',
    name: 'messagedetail',
    component: () => import('../views/messagedetail/index.vue'),
    children: [
      {
      path: 'tian',
      name: 'tian',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagedetail/tian.vue')
    },
      {
      path: 'tianer',
      name: 'tianer',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagedetail/tianer.vue')
    },
    {
      path: 'dong',
      name: 'dong',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagedetail/dong.vue')
    },
    {
      path: 'donger',
      name: 'donger',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagedetail/donger.vue')
    },
    {
      path: 'zixun',
      name: 'zixun',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagedetail/zixun.vue')
    },
    ]
  },
  {
    path: '/messagemy',
    name: 'messagemy',
    component: () => import('../views/messagemy/index.vue'),
    children: [{
      path: 'litianzemy',
      name: 'litianzemy',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagemy/litianzemy.vue')
    },
   
    {
      path: 'server',
      name: 'server',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagemy/server.vue')
    },
    {
      path: 'Hroute',
      name: 'Hroute',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagemy/Hroute.vue')
    },

    {
      path: 'baojia',
      name: 'baojia',
      meta: {
        hidden: true
      },
      component: () => import('../views/messagemy/baojia.vue')
    },
    ]
  },
  {
    path: '/hdetail',
    name: 'hdetail',
    component: () => import('../views/hdetail/index.vue'),
    children: [{
      path: 'ask',
      name: 'ask',
      component: () => import('../views/hdetail/ask.vue'),
    },
    {
      path: 'askcontent',
      name: 'askcontent',
      component: () => import('../views/hdetail/askcontent.vue'),
    },
    {
      path: 'content',
      name: 'content',
      meta: {
        hidden: true
      },
      component: () => import('../views/hdetail/content.vue'),
    },
    {
      path: 'serve',
      name: 'serve',
      component: () => import('../views/hdetail/serve.vue'),
    },
    {
      path: 'designer',
      name: 'designer',
      component: () => import('../views/hdetail/designer.vue'),
    },
    {
      path: 'album',
      name: 'album',
      meta: {
        hidden: true
      },
      component: () => import('../views/hdetail/album.vue'),
    },
    {
      path: 'release1',
      name: 'release1',
      meta: {
        hidden: true
      },
      component: () => import('../views/hdetail/release1.vue'),
    },
    {
      path: 'search',
      name: 'search',
      meta: {
        hidden: true
      },
      component: () => import('../views/hdetail/search.vue'),
    },
    {
      path: 'searcher',
      name: 'searcher',
      meta: {
        hidden: true
      },
      component: () => import('../views/hdetail/searcher.vue'),
    },
    ]
  },
  {
    path: '/person',
    name: 'person',
    component: () => import('../views/person/index.vue'),
    children: [{
      path: 'people',
      name: 'people',
      meta: {
        hidden: true
      },
      component: () => import('../views/person/people.vue'),
    },
    {
      path: 'personal',
      name: 'personal',
      meta: {
        hidden: true
      },
      component: () => import('../views/person/personal.vue'),
    }
    ]
  },
  {
    path: '/journey',
    name: 'journey',
    component: () => import('../views/journey/index.vue'),
    children: [
      {
        path: 'route',
        name: 'route',
        meta: {
          hidden: true
        },
        component: () => import('../views/journey/route.vue'),
      },
      {
        path: 'data',
        name: 'data',
        meta: {
          hidden: true
        },
        component: () => import('../views/journey/Data.vue'),
      },
      {
        path: 'routes',
        name: 'routes',
        meta: {
          hidden: true
        },
        component: () => import('../views/journey/routes.vue'),
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(
    import.meta.env.BASE_URL),
  routes
})
router.beforeEach((to, from) => {
  const userStore = useUserStore();
  if (to.fullPath === "/home") {
    router.push('/home/find')
  }
  if (to.fullPath == '/message') {
    router.push('/message/messagetell')
  }
  if (to.fullPath == '/details/receive') {
    router.push('/details/receive/receivego')
  }
  if (to.fullPath === "/message" || to.fullPath === "/my") {
          if (!userStore.userState) {
              router.push('/login')
          }
      }
});
export default router