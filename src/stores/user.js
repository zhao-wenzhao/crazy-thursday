import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const userState = ref(window.localStorage.getItem('userState') === 'true')
  // const userState = ref(false)
  function setUserState(val) {
    userState.value = val
  }
  const userId = ref(window.localStorage.getItem('userid') || '')
  // const userId = ref('')
  function setUserId(val) {
    userId.value = val
  }
  const username = ref(window.localStorage.getItem('username') || '')
  // const userId = ref('')
  function setUserName(val) {
    username.value = val
  }
  const userPhone = ref(window.localStorage.getItem('userphone') || '')
  function setUserPhone(val) {
    userPhone.value = val
  }
  const apply = ref(null)
  function setApply(val) {
    apply.value = val
  }
  const activeMessage = ref(null)
  function setActiveMessage(val) {
    activeMessage.value = val
  }
  const appDetail = ref(null)
  function setAppDetail(val) {
    appDetail.value = val
  }
  const searchbox = ref(null)
  function setSearchBox(val) {
    searchbox.value = val
  }
  return {
    searchbox,
    setSearchBox,
    userState,
    setUserState,
    username,
    setUserName,
    setUserId,
    userId,
    setUserPhone,
    userPhone,
    activeMessage,
    setActiveMessage,
    apply,
    setApply,
    setAppDetail,
    appDetail
  }
})