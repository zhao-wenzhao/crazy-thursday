
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import 'normalize.css/normalize.css'

import App from './App.vue'
import router from './router'
// Toast
import 'vant/es/toast/style';
// Dialog
import 'vant/es/dialog/style';

// Notify
import 'vant/es/notify/style';

// ImagePreview
import 'vant/es/image-preview/style';

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
