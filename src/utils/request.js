// axios 的二次封装
import axios from "axios";

// 创建一个我们自己的 'axios'
const instance = axios.create({
    baseURL: "/",
    timeout: 60000,
});
// 封装一个我们自己基于 instance 的请求函数
export function ajax(options) {
    const { url, method = "get", data = {} } = options;

    if (method.toUpperCase() === "GET") {
        // get 请求
        return instance.get(url, { params: data });
    } else {
        // post 请求
        return instance.post(url, data);
    }
}