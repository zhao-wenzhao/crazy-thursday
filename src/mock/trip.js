import Mock from 'mockjs'
import { Random } from 'mockjs'
let data=Mock.mock({
    'triplist|5':[
        {
            text:'@ctitle(3, 5)',
            image:Random.image('70x70'),
            "number|1-20": 10,
            'text2|1':['包车','轮渡'],
            tripid:'@id()',
            text3:'@cparagraph()',
            image1:Random.dataImage('375x200'),
            image2:Random.dataImage('375x200'),
            image3:Random.dataImage('375x200'),
            image4:Random.dataImage('375x200'),
        }
    ]
})
Mock.mock('/trip/tripedit','post',()=>{
    return data
}
)
Mock.mock('/trip/tripdetail','post',(config)=>{
    // console.log(JSON.parse(config).tripid);
    let data2=data.triplist.find(item=>{
        return item.tripid===JSON.parse(config.body).tripid
    })
    return data2
}
)