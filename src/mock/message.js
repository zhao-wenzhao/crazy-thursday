import Mock from 'mockjs'
//通知消息
let data=Mock.mock({
  'list|6':[
    {
      name:'@cname()',
      text:'@csentence()',
      time:'@time("H:m:s")',
      messageid:'@id()',
      'id|+1':0,
      tette:'@csentence(3, 5)',
      times:'@datetime',
      'num1|100-500':14,
      'num2|100-500':14,
      'num3|1000-5000':14,
    }
  ]
})
//出游定制
let data3=Mock.mock({
  'list|3':[
    {
      name:'@cname()',
      text:'@csentence()',
      time:'@time("H:m:s")',
      messageid:'@id()',
      'id|+1':0,
      titel:'@cparagraph()'
    }
  ]
})

//聊天页面
let data6=Mock.mock({
  'list|1':[
    {
      text:'@csentence()',
      messageid:'@id()',
      'id|+1':0,
      

    }
  ]
})
//咨询
let data7=Mock.mock({
  'list|1':[
    {
      name:'@cname()',
      text:'@csentence()',
      time:'@time("H:m:s")',
      messageid:'@id()',
      'id|+1':0,
      titel:'@cparagraph()',
      'num1|100-500':14,
      'num2|100-500':14,
      'num3|1000-5000':14,
    }
  ]
})
//定制页面

Mock.mock('/message/chat','post',()=>{
  return data
})
Mock.mock('/message/detail','post',(config)=>{                
  // console.log(JSON.parse(config.body));
  let data2=data.list.find(item=>{
    return item.messageid===JSON.parse(config.body).messageid
  })
  return data2
})



Mock.mock('/message/cftqa','post',()=>{
  return data3
})
Mock.mock('/message/litian','post',(config)=>{
 
  let data4=data3.list.find(item=>{
    // console.log(item.messageid);
    return item.messageid===JSON.parse(config.body).messageid
  })
return data4
})
//聊天页面
Mock.mock('/message/liaotian','post',()=>{
  return data6
})

//咨询
Mock.mock('/message/zixun','post',()=>{
  return data7
})
Mock.mock('/message/name','post',()=>{
  // let data8=data7.list.find(item=>{
  //   // console.log(item.messageid);
  //   return item.messageid===JSON.parse(config.body).messageid
  // })
return data7
})