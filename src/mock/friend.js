import Mock from 'mockjs'

const data = Mock.mock({
    'list|4-8': [{
        name: '@cname',
        text: '@csentence(8)',
        text2: '@csentence(5, 10)',
        time: '@now()',
        "number|20-300": 100,
        "num1|500-1000": 0,
        "num2|500-1000": 0,
        "num3|500-1000": 0,
    }]
})

const data1 = Mock.mock({
    name: '@cname',
    'list|4-8': [{
        name: '@cname',
        "boolean|1-2": true,
    }],
    time: '@time("H:m:s")',
    text: '@csentence()',
    tette: '@csentence(3, 5)',
    times: '@datetime'
})

Mock.mock('/getFriend', 'post', () => {
    return data
})

Mock.mock('/getAppDetail', 'post', () => {
    return data1
})

Mock.mock('/getSubmit', 'post', () => {
    return data1
})