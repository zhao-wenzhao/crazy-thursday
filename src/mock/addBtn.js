import Mock from 'mockjs'
let data=Mock.mock({
    'album|12':[{
        url:"https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg",
        imgid:'@id()',
        show:false
    }]
})
Mock.mock('/addBtn/album','post',()=>{
    return data
})