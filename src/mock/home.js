import Mock from 'mockjs'
import {
    Random
} from 'mockjs'
let data = Mock.mock({
    'tag|8': [{
        kind: '@ctitle(2, 4)',
        'careid|+1': 1,

    }]
})
let data2 = Mock.mock({
    'content|50': [{
        name: '@cname()',
        text: '@csentence(15, 20)',
        img: Random.dataImage('150x200'),
        'num|1000-2000': 500,
        flag: false,
        flag2: false,
        id: '@id()',
        text2: '@cparagraph(4, 5)',
        text3: '@cparagraph(4, 5)',
        text4: '@cparagraph(4, 5)',
        image1: Random.dataImage('375x200'),
        image2: Random.dataImage('375x200'),
        image3: Random.dataImage('375x200'),
        image4: Random.dataImage('375x200'),
        image5: Random.dataImage('375x200'),
        image6: Random.dataImage('375x200'),
        text9:'@csentence(7, 12)'


    }]
})
let data3 = Mock.mock({
    'content|50': [{
        name: '@cname()',
        text: '@csentence(30, 40)',
        img: Random.dataImage('150x350'),
        flag: false,
        text2: '@cparagraph(4, 5)',
        text3: '@cparagraph(4, 5)',
        text4: '@cparagraph(4, 5)',
        image1: Random.dataImage('375x200'),
        image2: Random.dataImage('375x200'),
        image3: Random.dataImage('375x200'),
        image4: Random.dataImage('375x200'),
    }]
})
let data4 = Mock.mock({
    'content|50': [{
        name: '@cname()',
        'num|500-1000': 10,
        'num2|2-10': 5,
        'num3|80-100': 5,
        img: Random.dataImage('100x70'),
        text: '@cparagraph(2)',
        id: '@id()',
        textbig: '@cparagraph()',
        'num1|100-200': 10,
        'num2|500-999': 10,
        'num3|2000-9999': 10,
    }]
})
let data5 = [
    '澳门',
    '厦门',
    '北京',
    '河南',
    '深圳',
]
let data6 = [
    '内蒙古',
    '北京',
    '哈尔滨',
    '山西',
    '西双版纳',
]
Mock.mock('/home/findlist', 'post', () => {
    return data
})
Mock.mock('/home/contentlist', 'post', () => {
    return data2
})
Mock.mock('/home/addcontentlist', 'post', (config) => {
    // console.log(333);
    // console.log(JSON.parse(config.body).shuju);
    data2.content.unshift(JSON.parse(config.body).shuju)
    return {}
})
Mock.mock('/home/editcontentlist', 'post', (config) => {
    // console.log(JSON.parse(config.body).content);
    let data21 = data2.content.find(item => {
        return item.id === JSON.parse(config.body).content.id
    })
    data21.flag = true
    data21.num1++
    data3.content.unshift(JSON.parse(config.body).content)
    return {
        code: 1,
        message: '已经关注成功'
    }
})
Mock.mock('/home/editcontentlister', 'post', (config) => {
    let data21 = data2.content.find(item => {
        return item.id === JSON.parse(config.body).content.id
    })
    data21.flag = false
    data3.content.shift()
    data21.num1--
    return {
        code: 1,
        message: '取消关注成功'
    }
})
Mock.mock('/home/carelist', 'post', (config) => {
    let count=JSON.parse(config.body).count
    let limitNum=JSON.parse(config.body).limitNum
    // console.log(count,limitNum);
    let data31=data3.content.slice((count-1)*limitNum,count*limitNum)
    return data31
})
Mock.mock('/home/peoplelist', 'post', () => {
    return data4
})
Mock.mock('/hdetail/search', 'post', () => {
    return data5
})
Mock.mock('/hdetail/searcher', 'post', () => {
    return data6
})
Mock.mock('/hdetail/editsearch', 'post', (config) => {
    data5.push(JSON.parse(config.body).name)
    return
})
Mock.mock('/hdetail/editsearcher', 'post', () => {
    data5=[]
    return
})