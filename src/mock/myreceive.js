import Mock from 'mockjs'
import { Random } from 'mockjs'
let data=Mock.mock({
        'orderlist|6':[
            {
                order:'@cword(3, 4)',
                orderid: '@id()',
            }
    ]
})
let data3=Mock.mock({
        'orderlist|4':[
            {
                order:'@cword(3, 4)',
                orderid: '@id()',
            }
    ]
})
let data2=Mock.mock({
        'addresslist|15':[
            {
                address:'@cword(4,6)',
                messageid: '@id()',
                message:'@cword(4,6)',
                time1:'@date()',
                time2:'@date()',
                'tag1|2-4':{
                    tagid1:'行程编辑',
                    tagid2:'行程修改',
                    tagid3:'沟通需求',
                    tagid4:'增加服务',
                }

            }
    ]
})
let data4=Mock.mock({
        'addresslist|15':[
            {
                address:'@cword(2,4)',
                messagekind: '电话沟通15分钟',
                message:'出行交通等三项',
                messageid: '@id()',
                time1:'@date()',
                'tag1|1-3':{
                    tagid1:'查看详情',
                    tagid2:'联系客户',
                    tagid3:'接单',
                }

            }
    ]
})
Mock.mock('/receivego/getorder','post',()=>{
    return data
}
)
Mock.mock('/receivego/getaddress','post',()=>{
    return data2
}
)
Mock.mock('/receiveserve/getorder','post',()=>{
    return data3
}
)
Mock.mock('/receiveserve/getaddress','post',()=>{
    return data4
}
)