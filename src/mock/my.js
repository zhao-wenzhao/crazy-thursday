import Mock from 'mockjs'
let data = Mock.mock({
    'data|6': [{
        name: '@cname()',
        'text|1': ['等待接单', '沟通中','已完成'],
        time: ['@date("yyyy-MM-dd")', '@date("yyyy-MM-dd")'],
        address:'@province()',
        times:'@datetime("M-d H:m")',
        'category|1':['出行','交通','花费','出行，交通等三项'],
        'order|1':['等待咨询','已完成','待评价'],
        'message|1':['电话沟通15分钟','发短信','qq','微信'],
        id:'@id()',
        'num1|100-400': 14,
        'num2|100-800': 23,
        'num3|1000-3000': 2343,
        name2: '@cname()',
        titel:'@csentence',
        title:'@csentence()'
    }]
})
let data2 = Mock.mock({
    'data|1':[{
        name:' 李泽天为你定制的6天5页大理之行',
        times:'2023.06.18',
        time:'2023.06.18-2023.06-24    (共六天)',
        title:'李泽天为您制定的6天5页大理之行',
        img:'',
        
    
        
    
    }]

})
let data3 = Mock.mock({
    'data|1':[{
        title:'十月摩托环岛自驾游',
        area:'@city()',
        address:'@county(true)',
        time:'@datetime()',
        titel:'@csentence',
        times:'@csentence()',
        name:'李泽天',
    }]
})
let data4 = Mock.mock({
    'data|1':[{
        name:'@cname()',
        'num1|100-400': 14,
        'num2|100-800': 23,
        'num3|1000-3000': 2343,
        title:'如果说读书是探索与思考世界的最佳方式,那旅行便是与真实世界最直观的碰撞与体验，旅行，是心灵的阅读，而阅读，是智慧的升华'
    }]
})
let data5 = Mock.mock({
    'data|1':[{
        name:'@cname()',
        area:'擅长4个目的地',
        goods:['户外探索','情侣蜜月'],
        address:['北京','上海','广州','深圳'],
        title:'【今日头条优质旅游领域创作者】马蜂窝攻略达人,指路人,4年的自由行经验,行走了国内35座城市,做国内旅游路线高端定制专属于您的自由行，小童猫有丰富的的旅行经验。带您开启一程不一样的旅行！',
        title1:'如果说读书是探索与思考世界的最佳方式，那旅行便是与真实世界最直观的碰撞与体验，旅行，是心灵的阅读。而阅读，是智慧的升华',
        dada:'服务200次',
        data1:'评价20条',
        good:'92.9%'

    }]
})
let data6 = Mock.mock({
    'data|6':[{
        "number|+1": 1,
        "number2|+1": 17
    }]
})

Mock.mock('/details/mytrip', 'post', () => {
    return data
})
Mock.mock('/details/myorder','post',()=>{
    return data2
})

Mock.mock('/details/myorder','get',()=>{
    return data3
})
Mock.mock('/person/people','post',()=>{
    return data4
})
Mock.mock('/person/personal','post',()=>{
    return data5
})
Mock.mock('/journey/route','post',()=>{
    return data6
})