import Mock from 'mockjs'
let data = {
    users: [{
        username: 'admin',
        password: '123456'
    }, ]
}
let token = Mock.mock({
    num: '@id()'
})
let code = Mock.mock({
    code: '@natural(10000, 99999)'
})
Mock.mock('/login/enter', 'post', (config) => {
    let content = data.users.find((item) => {
        return item.username === JSON.parse(config.body).username && item.password === JSON.parse(config.body).password
    })
    return content === undefined ? {
        code: 0,
        message: '用户不存在'
    } : {
        code: 1,
        message: '登录成功',
        data: {
            token: token.num,
            username: content.username,
            password:content.password
        }
    }
})
Mock.mock('/login/register', 'post', (config) => {
    let content = data.users.find((item) => {
        return item.username === JSON.parse(config.body).username
    })
    if (content) {
        return {
            code: 0,
            message: '账号已经存在'
        }
    } else {
        data.users.push({
            username: JSON.parse(config.body).username,
            password: JSON.parse(config.body).password
        })
        return {
            code: 1,
            message: '注册成功',
            data: data.users
        }
    }
})
Mock.mock('/login/code', 'post', () => {
    return code
})
Mock.mock('/login/testcode', 'post', (config) => {
    return JSON.parse(config.body).code == code.code ? {
        code: 1,
        message: '登录成功',
        data: {
            token: token.num,
            phone: JSON.parse(config.body).phone
        }
    } : {
        code: 0,
        message: '登陆失败'
    }
})
Mock.mock('/details/editpassword', 'post', (config) => {
    let data9=data.users.find(item=>{
        return item.username===JSON.parse(config.body).username
    })
    data9.password=JSON.parse(config.body).password
    return {
        code:1,
        message:'修改成功',
        data:data9
    }
})