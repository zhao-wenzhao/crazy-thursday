import {ajax} from "@/utils/request"
import '@/mock/trip.js'
export function getTripEdit(){
    return ajax({
        url:'/trip/tripedit',
        method:'post',
    })
}
export function getTripDetail(data){
    return ajax({
        url:'/trip/tripdetail',
        method:'post',
        data
    })
}