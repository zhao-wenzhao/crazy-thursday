import {ajax} from "@/utils/request"
import '@/mock/login'
export function userLogin(data){
    return ajax({
        url:'/login/enter',
        method:'post',
        data
    })
}
export function editPassword(data){
    return ajax({
        url:'/details/editpassword',
        method:'post',
        data
    })
}
export function userRegister(data){
    return ajax({
        url:'/login/register',
        method:'post',
        data
    })
}
export function getCode(){
    return ajax({
        url:'/login/code',
        method:'post',
    })
}
export function getCodeLogin(data){
    return ajax({
        url:'/login/testcode',
        method:'post',
        data
    })
}
