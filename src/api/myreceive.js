import {ajax} from "@/utils/request"
import '@/mock/myreceive.js'
export function getOrderList(){
    return ajax({
        url:'/receivego/getorder',
        method:'post',
    })
}
export function getAddressList(){
    return ajax({
        url:'/receivego/getaddress',
        method:'post'
    })
}
export function getAddressListServe(){
    return ajax({
        url:'/receiveserve/getaddress',
        method:'post'
    })
}
export function getOrderListServe(){
    return ajax({
        url:'/receiveserve/getorder',
        method:'post'
    })
}