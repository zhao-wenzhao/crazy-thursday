import {ajax} from "@/utils/request"
import '@/mock/message'
export function getMessageList(){
    return ajax({
        url:'/message/chat',
        method:'post',
    })
}
//
export function getMessageDetail(data){
    return ajax({
        url:'/message/detail',
        method:'post',
        data
    })
}

export function getMessageCftqa(){
    return ajax({
        url:'/message/cftqa',
        method:'post',
    })
}
//

export function getMessageLitian(data){
    return ajax({
        url:'/message/litian',
        method:'post',
        data
    })
}
//聊天
export function getMessageLiaotian(){
    return ajax({
        url:'/message/liaotian',
        method:'post',
    })
}
//咨询
export function getMessagezixun(){
    return ajax({
        url:'/message/zixun',
        method:'post',
    })
}
export function getMessageName(){
    return ajax({
        url:'/message/name',
        method:'post',
    })
}