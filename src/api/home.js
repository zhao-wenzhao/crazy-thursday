import {ajax} from "@/utils/request"
import '@/mock/home'
export function getFindList(){
    return ajax({
        url:'/home/findlist',
        method:'post',
    })
}
export function getContentList(){
    return ajax({
        url:'/home/contentlist',
        method:'post',
    })
}
export function addContentList(data){
    return ajax({
        url:'/home/addcontentlist',
        method:'post',
        data
    })
}
export function getCareList(data){
    return ajax({
        url:'/home/carelist',
        method:'post',
        data
    })
}

export function getSearchList(){
    return ajax({
        url:'/hdetail/search',
        method:'post',
    })
}
export function editSearchList(data){
    return ajax({
        url:'/hdetail/editsearch',
        method:'post',
        data
    })
}
export function editSearchListEr(){
    return ajax({
        url:'/hdetail/editsearcher',
        method:'post',
    })
}
export function getSearchListEr(){
    return ajax({
        url:'/hdetail/searcher',
        method:'post',
    })
}
export function getPeopleList(){
    return ajax({
        url:'/home/peoplelist',
        method:'post',
    })
}
export function editContentList(data){
    return ajax({
        url:'/home/editcontentlist',
        method:'post',
        data
    })
}
export function editContentListEr(data){
    return ajax({
        url:'/home/editcontentlister',
        method:'post',
        data
    })
}