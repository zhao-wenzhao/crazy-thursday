import { ajax } from "@/utils/request"
import '@/mock/my'
export function getTrip(){
    return ajax({
        url:'/details/mytrip',
        method:'post',
    })
}
export function getOrder(){
    return ajax({
        url:'/details/myorder',
        method:'post'
    })
}
export function getMyOrder(){
    return ajax({
        url:'/details/myorder',
    })
}
export function getPeople(){
    return ajax({
        url:'/person/people',
        method:'post'
    })
}
export function getPensonal(){
    return ajax({
        url:'/person/personal',
        method:'post'
    })
}
export function getRoute(){
    return ajax({
        url:'/journey/route',
        method:'post'
    })
}