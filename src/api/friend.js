import { ajax } from "@/utils/request"
import '@/mock/friend'

export function getFriend() {
    return ajax({
        url: '/getFriend',
        method: 'post'
    })
}

export function getFriendActive() {
    return ajax({
        url: '/getFriendActive',
        method: 'post'
    })
}

export function getAppDetail() {
    return ajax({
        url: '/getAppDetail',
        method: 'post'
    })
}


export function getSubmit() {
    return ajax({
        url: '/getSubmit',
        method: 'post'
    })
}